node n0 {
    type router
    model static
    network-config {
	hostname routerA
	!
	interface eth1
	 ipv6 address fc00::1/64
	 mac address 42:00:aa:00:00:08
	 ip address 102.174.84.1/24
	!
	interface eth0
	 ip address 10.0.2.2/24
	 mac address 42:00:aa:00:00:05
	!
	interface lo0
	 type lo
	 ip address 127.0.0.1/8
	 ipv6 address ::1/128
	!
	router rip
	 redistribute static
	 redistribute connected
	 redistribute ospf
	 network 0.0.0.0/0
	!
	router ripng
	 redistribute static
	 redistribute connected
	 redistribute ospf6
	 network ::/0
	!
	ip route 0.0.0.0/0 10.0.2.1
	!
    }
    canvas c0
    iconcoords {120 168}
    labelcoords {120 193}
    interface-peer {eth0 n1}
    interface-peer {eth1 n5}
}

node n1 {
    type router
    model static
    network-config {
	hostname routerB
	!
	interface eth3
	 ipv6 address fc00:1::1/64
	 mac address 42:00:aa:00:00:0b
	 ip address 176.84.100.1/26
	!
	interface eth2
	 ip address 10.0.3.1/24
	 mac address 42:00:aa:00:00:06
	!
	interface eth1
	 ip address 10.0.2.1/24
	 mac address 42:00:aa:00:00:04
	!
	interface eth0
	 ip address 10.0.1.2/24
	 mac address 42:00:aa:00:00:03
	!
	interface lo0
	 type lo
	 ip address 127.0.0.1/8
	 ipv6 address ::1/128
	!
	router rip
	 redistribute static
	 redistribute connected
	 redistribute ospf
	 network 0.0.0.0/0
	!
	router ripng
	 redistribute static
	 redistribute connected
	 redistribute ospf6
	 network ::/0
	!
	ip route 102.174.84.0/24 10.0.2.2
	ip route 161.53.19.1/24 10.0.1.1
	ip route 86.100.174.0/28 10.0.3.2
	!
    }
    canvas c0
    iconcoords {480 192}
    labelcoords {524 153}
    interface-peer {eth0 n3}
    interface-peer {eth1 n0}
    interface-peer {eth2 n2}
    interface-peer {eth3 n6}
}

node n2 {
    type router
    model static
    network-config {
	hostname routerC
	!
	interface eth1
	 ipv6 address fc00:2::1/64
	 mac address 42:00:aa:00:00:10
	 ip address 86.100.174.1/28
	!
	interface eth0
	 ip address 10.0.3.2/24
	 mac address 42:00:aa:00:00:07
	!
	interface lo0
	 type lo
	 ip address 127.0.0.1/8
	 ipv6 address ::1/128
	!
	router rip
	 redistribute static
	 redistribute connected
	 redistribute ospf
	 network 0.0.0.0/0
	!
	router ripng
	 redistribute static
	 redistribute connected
	 redistribute ospf6
	 network ::/0
	!
	ip route 0.0.0.0/0 10.0.3.1
	!
    }
    canvas c0
    iconcoords {816 72}
    labelcoords {813 106}
    interface-peer {eth0 n1}
    interface-peer {eth1 n7}
}

node n3 {
    type router
    model static
    network-config {
	hostname routerX
	!
	interface eth1
	 ip address 10.0.1.1/24
	 mac address 42:00:aa:00:00:02
	!
	interface eth0
	 mac address 42:00:aa:00:00:00
	 ip address 161.53.19.1/24
	!
	interface lo0
	 type lo
	 ip address 127.0.0.1/8
	 ipv6 address ::1/128
	!
	router rip
	 redistribute static
	 redistribute connected
	 redistribute ospf
	 network 0.0.0.0/0
	!
	router ripng
	 redistribute static
	 redistribute connected
	 redistribute ospf6
	 network ::/0
	!
	ip route 0.0.0.0/0 10.0.1.2
	!
    }
    canvas c0
    iconcoords {480 48}
    labelcoords {499 15}
    interface-peer {eth0 n4}
    interface-peer {eth1 n1}
}

node n4 {
    type host
    network-config {
	hostname HOST
	!
	interface eth0
	 mac address 42:00:aa:00:00:01
	 ip address 161.53.19.8/24
	!
	interface lo0
	 type lo
	 ip address 127.0.0.1/8
	 ipv6 address ::1/128
	!
	ip route 0.0.0.0/0 161.53.19.1
	!
    }
    canvas c0
    iconcoords {96 48}
    labelcoords {96 84}
    interface-peer {eth0 n3}
}

node n5 {
    type lanswitch
    network-config {
	hostname switch1
	!
    }
    canvas c0
    iconcoords {168 336}
    labelcoords {168 376}
    interface-peer {e0 n0}
    interface-peer {e1 n8}
    interface-peer {e2 n9}
}

node n6 {
    type lanswitch
    network-config {
	hostname switch2
	!
    }
    canvas c0
    iconcoords {504 336}
    labelcoords {504 359}
    interface-peer {e0 n1}
    interface-peer {e1 n10}
    interface-peer {e2 n11}
}

node n7 {
    type lanswitch
    network-config {
	hostname switch3
	!
    }
    canvas c0
    iconcoords {792 288}
    labelcoords {792 311}
    interface-peer {e0 n12}
    interface-peer {e1 n13}
    interface-peer {e2 n2}
}

node n8 {
    type pc
    network-config {
	hostname pc1
	!
	interface eth0
	 ipv6 address fc00::20/64
	 mac address 42:00:aa:00:00:09
	 ip address 102.174.84.254/24
	!
	interface lo0
	 type lo
	 ip address 127.0.0.1/8
	 ipv6 address ::1/128
	!
	!
	ip route 0.0.0.0/0 102.174.84.1
	!
    }
    canvas c0
    iconcoords {48 528}
    labelcoords {48 559}
    interface-peer {eth0 n5}
}

node n9 {
    type pc
    network-config {
	hostname pc2
	!
	interface eth0
	 ipv6 address fc00::21/64
	 mac address 42:00:aa:00:00:0a
	 ip address 102.174.84.253/24
	!
	interface lo0
	 type lo
	 ip address 127.0.0.1/8
	 ipv6 address ::1/128
	!
	!
	ip route 0.0.0.0/0 102.174.84.1
	!
    }
    canvas c0
    iconcoords {312 528}
    labelcoords {312 559}
    interface-peer {eth0 n5}
}

node n10 {
    type pc
    network-config {
	hostname pc3
	!
	interface eth0
	 ipv6 address fc00:1::20/64
	 mac address 42:00:aa:00:00:0c
	 ip address 176.84.100.62/26
	!
	interface lo0
	 type lo
	 ip address 127.0.0.1/8
	 ipv6 address ::1/128
	!
	!
	ip route 0.0.0.0/0 176.84.100.1
	!
    }
    canvas c0
    iconcoords {384 552}
    labelcoords {384 583}
    interface-peer {eth0 n6}
}

node n11 {
    type pc
    network-config {
	hostname pc4
	!
	interface eth0
	 ipv6 address fc00:1::21/64
	 mac address 42:00:aa:00:00:0d
	 ip address 176.84.100.61/26
	!
	interface lo0
	 type lo
	 ip address 127.0.0.1/8
	 ipv6 address ::1/128
	!
	!
	ip route 0.0.0.0/0 176.84.100.1
	!
    }
    canvas c0
    iconcoords {624 528}
    labelcoords {624 559}
    interface-peer {eth0 n6}
}

node n12 {
    type pc
    network-config {
	hostname pc5
	!
	interface eth0
	 ipv6 address fc00:2::20/64
	 mac address 42:00:aa:00:00:0e
	 ip address 86.100.174.14/28
	!
	interface lo0
	 type lo
	 ip address 127.0.0.1/8
	 ipv6 address ::1/128
	!
	!
	ip route 0.0.0.0/0 86.100.174.1
	!
    }
    canvas c0
    iconcoords {696 552}
    labelcoords {696 583}
    interface-peer {eth0 n7}
}

node n13 {
    type pc
    network-config {
	hostname pc6
	!
	interface eth0
	 ipv6 address fc00:2::21/64
	 mac address 42:00:aa:00:00:0f
	 ip address 86.100.174.13/28
	!
	interface lo0
	 type lo
	 ip address 127.0.0.1/8
	 ipv6 address ::1/128
	!
	!
	ip route 0.0.0.0/0 86.100.174.1
	!
    }
    canvas c0
    iconcoords {864 528}
    labelcoords {864 559}
    interface-peer {eth0 n7}
}

link l0 {
    nodes {n3 n4}
    bandwidth 0
}

link l1 {
    nodes {n3 n1}
    bandwidth 0
}

link l2 {
    nodes {n1 n0}
    bandwidth 0
}

link l3 {
    nodes {n1 n2}
    bandwidth 0
}

link l4 {
    nodes {n0 n5}
    bandwidth 0
}

link l5 {
    nodes {n8 n5}
    bandwidth 0
}

link l6 {
    nodes {n9 n5}
    bandwidth 0
}

link l7 {
    nodes {n6 n1}
    bandwidth 0
}

link l8 {
    nodes {n10 n6}
    bandwidth 0
}

link l9 {
    nodes {n11 n6}
    bandwidth 0
}

link l10 {
    nodes {n12 n7}
    bandwidth 0
}

link l11 {
    nodes {n13 n7}
    bandwidth 0
}

link l12 {
    nodes {n7 n2}
    bandwidth 0
}

canvas c0 {
    name {Canvas0}
}

option show {
    interface_names yes
    ip_addresses yes
    ipv6_addresses yes
    node_labels yes
    link_labels yes
    background_images no
    annotations yes
    hostsAutoAssign no
    grid yes
    iconSize normal
    zoom 1.0
}

